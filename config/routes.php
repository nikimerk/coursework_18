<?php

return array(
    'requisites/del/([0-9]+)' => 'requisites/del/$1',
    'requisites/update/([0-9]+)' => 'requisites/update/$1',
    'requisites/add' => 'requisites/add',
    'requisites' => 'requisites/index',

    'company/del/([0-9]+)' => 'company/del/$1',
    'company/update/([0-9]+)' => 'company/update/$1',
    'company/add' => 'company/add',
    'company' => 'company/index',

    'document/del/([0-9]+)' => 'document/del/$1',
    'document/update/([0-9]+)' => 'document/update/$1',
    'document/download/([0-9]+)' => 'document/download/$1',
    'document/create' => 'document/create',
    'dump' => 'document/dump',
    'document' => 'document/index',

    'user/toadmin/([0-9]+)' => 'user/toadmin/$1',
    'user/deluser/([0-9]+)' => 'user/deluser/$1',
    'user/deladmin/([0-9]+)' => 'user/deladmin/$1',
    'user/viewuser' => 'user/viewuser',
    'user/register' => 'user/register',
    'user/login' => 'user/login',
    'user/logout' => 'user/logout',
    'user' => 'user/login',

    'cabinet/edit' => 'cabinet/edit',
    'cabinet/view/([0-9]+)' => 'cabinet/view/$1',
    'cabinet/delete/([0-9]+)' => 'cabinet/delete    /$1',
    'cabinet/shop_list' =>'cabinet/list',
    'cabinet' => 'cabinet/index',

    '' => 'cabinet/index', // actionIndex в SiteController  
);
