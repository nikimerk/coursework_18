<?php

class Admin {
    public static function ItsAdmin($id){

        $db = Db::getConnection();

        $sql = 'SELECT role FROM user WHERE id=:id';
        $result = $db->prepare($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $result ->bindParam(':id',$id, PDO::PARAM_INT);
        $result->execute();

        if($result->fetch()['role'] == "admin"){
            return true;
        }

            header("Location: /cabinet/");
            die;

    }

}