<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 03.05.2018
 * Time: 12:45
 */

class Company
{
    public static function Add($Name, $Address)
    {
        $db = Db::getConnection();

        $sql = 'INSERT INTO company (Name, Address) VALUES (:Name, :Address)';

        $result = $db->prepare($sql);
        $result->bindParam(':Name', $Name, PDO::PARAM_STR);
        $result->bindParam(':Address', $Address, PDO::PARAM_STR);

        return $result->execute();
    }

    public static function getCompanyList()
    {
        $db = Db::getConnection();

        $companyList = array();

        $sql = ('SELECT * FROM company');
        $result = $db->prepare($sql);
        $result->execute();

        $i=0;
        while ($row = $result->fetch()) {
            $companyList[$i]['id'] = $row['id'];
            $companyList[$i]['Name'] = $row['Name'];
            $companyList[$i]['Address'] = $row['Address'];
            $i++;
        }
        return $companyList;
    }

    public static function DelCompany($id)
    {
        $db= Db::getConnection();
        $sql = ('DELETE FROM `company` WHERE id=:id');
        $result = $db->prepare($sql);
        $result->bindParam(':id',$id, PDO::PARAM_INT);
        $result->execute();
    }

    public static function saveCompany($id, $Name, $Address)
    {
        $db = Db::getConnection();

        $sql = 'UPDATE company SET Name=:Name, Address=:Address WHERE id=:id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':Name', $Name, PDO::PARAM_STR);
        $result->bindParam(':Address', $Address, PDO::PARAM_STR);

        return  $result->execute();
    }
}