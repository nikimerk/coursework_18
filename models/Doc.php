<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 26.04.2018
 * Time: 16:08
 */

class Doc
{
    public static function GetAllDocuments()
    {
        $db = Db::getConnection();

        $documentsList = array();

        $result = $db->query('SELECT * FROM document');
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        while ($row = $result->fetch()) {
            $documentsList[$i]['id'] = $row['id'];
            $documentsList[$i]['Create_date'] = $row['Create_date'];
            $documentsList[$i]['Name'] = $row['Name'];
            $documentsList[$i]['Edit_date'] = $row['Edit_date'];
            $documentsList[$i]['Author_id'] = $row['Author_id'];
//            print_r($documentsList[$i]['id'] = $row['id']);
            $documentsList[$i]['Doc_type_id'] = $row['Doc_type_id'];
            $documentsList[$i]['Status_id'] = $row['Status_id'];
            $documentsList[$i]['Addressante_id'] = $row['Addressante_id'];
            $documentsList[$i]['Doc_fields_id'] = $row['Doc_fields_id'];
            $documentsList[$i]['Doc_req_id'] = $row['Doc_req_id'];
            $i++;
        }

        return $documentsList;

    }

    public static function getDocumentById($id)
    {
        $db = Db::getConnection();

        $document = array();

        $sql = ('SELECT * FROM document WHERE id=:id');
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id);
        $result->execute();
        while ($row = $result->fetch()) {
            $document['id'] = $row['id'];
            $document['Create_date'] = $row['Create_date'];
            $document['Name'] = $row['Name'];
            $document['Edit_date'] = $row['Edit_date'];
            $document['Author_id'] = $row['Author_id'];
            $document['Doc_type_id'] = $row['Doc_type_id'];
            $document['Status_id'] = $row['Status_id'];
            $document['Addressante_id'] = $row['Addressante_id'];
            $document['Doc_fields_id'] = $row['Doc_fields_id'];
            $document['Doc_req_id'] = $row['Doc_req_id'];
        }

        return $document;
    }

    public static function getFieldById($id)
    {
        $db = Db::getConnection();

        $field = array();

        $sql = ('SELECT * FROM fields WHERE id=:id');
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id);
        $result->execute();
        while ($row = $result->fetch()) {
            $field['id'] = $row['id'];
            $field['Name'] = $row['Name'];
            $field['fields_type'] = $row['fields_type'];
        }

        return $field;
    }

    public static function getStatusById($id)
    {
        $db = Db::getConnection();
        $sql = ('SELECT * FROM doc_status WHERE id=:id');
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id);
        $result->execute();
        $status = $result->fetch();

        return $status[1];
    }

    public static function getStatusByIdALL($id)
    {
        $db = Db::getConnection();
        $sql = ('SELECT * FROM doc_status WHERE id=:id');
        $result = $db->prepare($sql);
        $status = array();
        $result->bindParam(':id', $id);
        $result->execute();
        while ($row = $result->fetch()) {
            $status['id'] = $row['id'];
            $status['Status'] = $row['Status'];
        }
        return $status;
    }

    public static function getStatusList()
    {
        $db = Db::getConnection();

        $statusList = array();

        $sql = ('SELECT * FROM doc_status');
        $result = $db->prepare($sql);
        $result->execute();

        $i=0;
        while ($row = $result->fetch()) {
            $statusList[$i]['id'] = $row['id'];
            $statusList[$i]['Status'] = $row['Status'];
            $i++;
        }
        return $statusList;
    }

    public static function getStatus()
    {
        $db = Db::getConnection();

        $status = array();

        $sql = ('SELECT * FROM doc_status');
        $result = $db->prepare($sql);
        $result->execute();


        while ($row = $result->fetch()) {
            $status[$row['id']] = $row['Status'];

        }
        return $status;
    }

    public static function getTypeList()
    {
        $db = Db::getConnection();

        $DocTypeList = array();

        $sql = ('SELECT * FROM doc_type');
        $result = $db->prepare($sql);
        $result->execute();

        $i=0;
        while ($row = $result->fetch()) {
            $DocTypeList[$i]['id'] = $row['id'];
            $DocTypeList[$i]['Name'] = $row['Name'];
            $DocTypeList[$i]['Link'] = $row['link'];
            $i++;
        }
        return $DocTypeList;
    }

    public static function getTypeById($id)
    {
        $db = Db::getConnection();

        $type = array();

        $sql = ('SELECT * FROM doc_type WHERE id=:id');
        $result = $db->prepare($sql);
        $result ->bindParam(':id', $id);
        $result->execute();

        while ($row = $result->fetch()) {
            $type['id'] = $row['id'];
            $type['Name'] = $row['Name'];
            $type['Link'] = $row['link'];
        }
        return $type;
    }

    public static function getCompanyById($id)
    {
        $db = Db::getConnection();

        $company = array();

        $sql = ('SELECT * FROM company WHERE id=:id');
        $result = $db->prepare($sql);
        $result ->bindParam(':id', $id);
        $result->execute();

        while ($row = $result->fetch()) {
            $company['id'] = $row['id'];
            $company['Name'] = $row['Name'];
            $company['Address'] = $row['Address'];
        }
        return $company;
    }

//    public static function getCompanyById($id)
//    {
//        $db = Db::getConnection();
//
//        $status = array();
//
//        $sql = ('SELECT * FROM company WHERE id=:id');
//        $result = $db->prepare($sql);
//        $result->bindParam(':id', $id);
//        $result->execute();
//
//        while ($row = $result->fetch()) {
//            $status[$] = $row['Status'];
//            $status[$i] = $row['Status'];
//            $status[$i] = $row['Status'];
//            $status[$i] = $row['Status'];
//
//        }
//        return $status;
//    }

    public static function getOccupationsList ()
    {
        $db = Db::getConnection();

        $occupationsList = array();

        $sql = ('SELECT * FROM occupations');
        $result = $db->prepare($sql);
        $result->execute();

        $i=0;
        while ($row = $result->fetch()) {
            $occupationsList[$i]['id'] = $row['id'];
            $occupationsList[$i]['Name'] = $row['Name'];
            $i++;
        }
        return $occupationsList;
    }

    public static function getOccupationsById($id)
    {
        $db = Db::getConnection();

        $occupation = array();

        $sql = ('SELECT * FROM occupations WHERE id=:id');
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id);
        $result->execute();

        while ($row = $result->fetch()) {
            $occupation['id'] = $row['id'];
            $occupation['Name'] = $row['Name'];

        }
        return $occupation;
    }

    public static function saveFields($name)
    {
        $db = Db::getConnection();

        $sql = 'INSERT INTO fields (Name) VALUES (:Name)';
        $result = $db->prepare($sql);
        $result->bindParam(':Name', $name, PDO::PARAM_STR);
        $result->execute();
        return $db->lastInsertId();
    }

    public static function saveFieldById($id, $name)
    {
        $db = Db::getConnection();

        $sql = 'UPDATE fields SET Name = :Name WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':Name', $name, PDO::PARAM_STR);

        return $result->execute();
    }

    public static function delFieldById($id)
    {
        $db= Db::getConnection();
        $sql = ('DELETE FROM fields WHERE id=:id');
        $result = $db->prepare($sql);
        $result->bindParam(':id',$id, PDO::PARAM_INT);
        $result->execute();
    }


    public static function saveDocument($name, $edit_date, $author_id, $doc_type_id, $addressante_id, $doc_fields_id )
    {

//        print_r($name.'  | '.$edit_date.'  | '.$author_id.'  | '.$doc_type_id.'  | '.$addressante_id.'  | '.$doc_fields_id);
        $db = Db::getConnection();

        $sql = 'INSERT INTO document (Name, Edit_date, Author_id, Doc_type_id, Addressante_id, Doc_fields_id)'
            .'VALUES (:Name, :Edit_date, :Author_id, :Doc_type_id, :Addressante_id, :Doc_fields_id)';

        $result = $db->prepare($sql);
        $result->bindParam(':Name', $name, PDO::PARAM_STR);
        $result->bindParam(':Edit_date', $edit_date, PDO::PARAM_STR);
        $result->bindParam(':Author_id', $author_id, PDO::PARAM_INT);
        $result->bindParam(':Doc_type_id', $doc_type_id, PDO::PARAM_INT);
        $result->bindParam(':Addressante_id', $addressante_id, PDO::PARAM_INT);
        $result->bindParam(':Doc_fields_id', $doc_fields_id, PDO::PARAM_INT);

        return $result->execute();
    }

    public static function updateDocument($id, $name, $edit_date, $author_id, $doc_type_id, $addressante_id, $status_id)
    {

//        print_r($id.' | '.$name.'  | '.$edit_date.'  | '.$author_id.'  | '.$doc_type_id.'  | '.$addressante_id);
        $db = Db::getConnection();

        $sql = 'UPDATE document SET Name = :Name , Edit_date = :Edit_date , Author_id = :Author_id , Doc_type_id = :Doc_type_id , Status_id = :Status_id , Addressante_id = :Addressante_id  WHERE id=:id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':Name', $name, PDO::PARAM_STR);
        $result->bindParam(':Edit_date', $edit_date, PDO::PARAM_STR);
        $result->bindParam(':Author_id', $author_id, PDO::PARAM_INT);
        $result->bindParam(':Status_id', $status_id, PDO::PARAM_INT);
        $result->bindParam(':Doc_type_id', $doc_type_id, PDO::PARAM_INT);
        $result->bindParam(':Addressante_id', $addressante_id, PDO::PARAM_INT);

        return $result->execute();
    }

    public static function DelDocument($id)
    {
        $db= Db::getConnection();
        $sql = ('DELETE FROM `document` WHERE id=:id');
        $result = $db->prepare($sql);
        $result->bindParam(':id',$id, PDO::PARAM_INT);
        $result->execute();
    }



}