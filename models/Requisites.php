<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 27.04.2018
 * Time: 7:59
 */

class Requisites
{
    public static function Add($phone, $fullname, $email, $fax, $company_id, $Occupations_id)
    {
        $db = Db::getConnection();

        $sql = 'INSERT INTO requisites (Phone, Fullname, Email, Fax, Company_id, Occupations_id) '
            . 'VALUES (:Phone, :Fullname, :Email, :Fax, :Company_id, :Occupations_id)';

        $result = $db->prepare($sql);
        $result->bindParam(':Phone', $phone, PDO::PARAM_STR);
        $result->bindParam(':Fullname', $fullname, PDO::PARAM_STR);
        $result->bindParam(':Email', $email, PDO::PARAM_STR);
        $result->bindParam(':Fax', $fax, PDO::PARAM_STR);
        $result->bindParam(':Company_id', $company_id, PDO::PARAM_STR);
        $result->bindParam(':Occupations_id', $Occupations_id, PDO::PARAM_STR);

        return $result->execute();
    }

    public static function getRequisitesList ()
    {
        $db = Db::getConnection();

        $requisitesList = array();

        $sql = ('SELECT * FROM requisites');
        $result = $db->prepare($sql);
        $result->execute();

        $i=0;
        while ($row = $result->fetch()) {
            $requisitesList[$i]['id'] = $row['id'];
            $requisitesList[$i]['Phone'] = $row['Phone'];
            $requisitesList[$i]['Fullname'] = $row['Fullname'];
            $requisitesList[$i]['Email'] = $row['Email'];
            $requisitesList[$i]['Fax'] = $row['Fax'];
            $requisitesList[$i]['Company_id'] = $row['Company_id'];
            $requisitesList[$i]['Occupations_id'] = $row['Occupations_id'];
            $i++;
        }
        return $requisitesList;
    }

    public static function getRequisiteById ($id)
    {
        $db = Db::getConnection();

        $requisite = array();

        $sql = ('SELECT * FROM requisites WHERE id=:id');
        $result = $db->prepare($sql);
        $result ->bindParam(':id', $id);
        $result->execute();

        while ($row = $result->fetch()) {
            $requisite['id'] = $row['id'];
            $requisite['Phone'] = $row['Phone'];
            $requisite['Fullname'] = $row['Fullname'];
            $requisite['Email'] = $row['Email'];
            $requisite['Fax'] = $row['Fax'];
            $requisite['Company_id'] = $row['Company_id'];
            $requisite['Occupations_id'] = $row['Occupations_id'];

        }
        return $requisite;
    }

    public static function saveRequisites($id, $phone, $fullname, $Email, $Fax, $Company_id, $Occupations_id)
    {
        $db = Db::getConnection();

        $sql = "UPDATE requisites SET Phone=:Phone, Fullname=:Fullname, Email=:Email, Fax=:Fax, Company_id=:Company_id, Occupations_id=:Occupations_id WHERE id=:id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':Phone', $phone, PDO::PARAM_STR);
        $result->bindParam(':Fullname', $fullname, PDO::PARAM_STR);
        $result->bindParam(':Email', $Email);
        $result->bindParam(':Fax', $Fax, PDO::PARAM_STR);
        $result->bindParam(':Company_id', $Company_id, PDO::PARAM_INT);
        $result->bindParam(':Occupations_id', $Occupations_id, PDO::PARAM_INT);
//          ($result->execute());
        return  $result->execute();
    }

    public static function DelReqisites($id)
    {
        $db= Db::getConnection();
        $sql = ('DELETE FROM `requisites` WHERE id=:id');
        $result = $db->prepare($sql);
        $result->bindParam(':id',$id, PDO::PARAM_INT);
        $result->execute();
    }


}