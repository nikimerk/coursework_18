<?php
    require 'vendor/autoload.php';
    
    use PHPStamp\Templator;
    use PHPStamp\Document\WordDocument;

    const cachePath = 'C:/Xampp/htdocs/kurs/php_stamp/static/';

    function createDocument($doc, $values) {
        $path = cachePath .  md5(rand (0 , 40000)) . '/';
        mkdir($path);

        try {
            $templator = new Templator($path);
            $document = new WordDocument($doc);
        } catch (Exception $e) {
            return false;
        }

        $result = $templator->render($document, $values);

//        echo "<pre>";
//        print_r($result);
        $result->download();

        rmdir($path);

        return true;
    }