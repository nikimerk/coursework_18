<?php

class UserController
{

    public function actionRegister()
    {
        $name = '';
        $email = '';
        $password = '';
        $result = false;
        
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            
            $errors = false;
            
            if (!User::checkName($name)) {
                $errors[] = 'Имя не должно быть короче 2-х символов';
            }
            
            if (!User::checkEmail($email)) {
                $errors[] = 'Неправильный email';
            }
            
            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 6-ти символов';
            }
            
            if (User::checkEmailExists($email)) {
                $errors[] = 'Такой email уже используется';
            }
            
            if ($errors == false) {
                $result = User::register($name, $email, $password);
            }

        }

        require_once(ROOT . '/views/user/register.php');

        return true;
    }

    public function actionLogin()
    {
        $email = '';
        $password = '';
        
        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];
            
            $errors = false;
                        
            // Валидация полей
            if (!User::checkEmail($email)) {
                $errors[] = 'Неправильный email';
            }            
            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 6-ти символов';
            }
            
            // Проверяем существует ли пользователь
            $userId = User::checkUserData($email, $password);
            
            if ($userId == false) {
                // Если данные неправильные - показываем ошибку
                $errors[] = 'Неправильные данные для входа на сайт';
            } else {
                // Если данные правильные, запоминаем пользователя (сессия)
                User::auth($userId);
                
                // Перенаправляем пользователя в закрытую часть - кабинет 
                header("Location: /cabinet");
            }

        }

        require_once(ROOT . '/views/user/login.php');

        return true;
    }
    
    /**
     * Удаляем данные о пользователе из сессии
     */
    public function actionLogout()
    {
        session_start();
        unset($_SESSION["user"]);
        header("Location: /");
    }

    public function ActionViewuser()
    {
        User::checkAdmin();
        $userList = User::getUserList();

        require_once (ROOT.'/views/cabinet/ViewUser.php');
        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function ActionToadmin($id)
    {
        User::checkAdmin();
        $db = Db::getConnection();
        $role = 'admin';
        $sql = 'UPDATE user SET `role`=:role WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':role', $role, PDO::PARAM_STR);;
        $result->bindParam(':id', $id, PDO::PARAM_INT);;
        print_r($result->execute());
        header("Location: /user/viewuser");
        return true;
    }

    public function ActionDeladmin($id)
    {
        User::checkAdmin();
        $db = Db::getConnection();
        $role = 'user';
        $sql = 'UPDATE user SET `role`=:role WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':role', $role, PDO::PARAM_STR);;
        $result->bindParam(':id', $id, PDO::PARAM_INT);;
        print_r($result->execute());
        header("Location: /user/viewuser");
        return true;
    }

    public function ActionDeluser($id)
    {
        User::checkAdmin();
        $db = Db::getConnection();

        $sql = 'DELETE FROM `user` WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);;
        print_r($result->execute());
        header("Location: /user/viewuser");
        return true;
    }
}


