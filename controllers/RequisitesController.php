<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 27.04.2018
 * Time: 7:47
 */

class RequisitesController
{
    public function ActionIndex(){
        User::checkAdmin();

        $requisitesList = Requisites::getRequisitesList();

        require_once (ROOT.'/views/panel/Reqisites/Index.php');
        return true;
    }

    public function ActionAdd()
    {
        User::checkAdmin();

        $occupationsList = Doc::getOccupationsList();
        $companyList = Company::getCompanyList();
        $Phone = '';
        $Fullname = '';
        $Email = '';
        $Fax = '';
        $Company_id = '';
        $Occupations_id = '';
        $result = false;

        if (isset($_POST['submit'])) {
            $Phone = $_POST['Phone'];
            $Fullname = $_POST['Fullname'];
            $Email = $_POST['Email'];
            $Fax = $_POST['Fax'];
            $Company_id = $_POST['Company_id'];
            $Occupations_id = $_POST['Occupations_id'];
            $errors = false;
            if (!User::checkName($Fullname)) {
                $errors[] = 'Имя не должно быть короче 2-х символов';
            }
            if (!User::checkEmail($Email)) {
                $errors[] = 'Неправильный email';
            }
            if ($errors == false) {
                $result = Requisites::Add($Phone, $Fullname, $Email, $Fax, $Company_id, $Occupations_id);
            }
        }
        require_once(ROOT . '/views/panel/Reqisites/Add.php');
        return true;
    }

    public function ActionUpdate($id)
    {
        User::checkAdmin();


        $requisite =  Requisites::getRequisiteById($id);;
        $occupations2 = Doc::getOccupationsById($requisite['Occupations_id']);
        $occupationsList = Doc::getOccupationsList();
        $company2 = Doc::getCompanyById($requisite['Company_id']);
        $companyList = Company::getCompanyList();

        if(isset($_POST['submit'])){

            $Phone = $_POST['Phone'];
            $Fullname = $_POST['Fullname'];
            $Email = $_POST['Email'];
            $Fax = $_POST['Fax'];
            $Company_id = $_POST['Company_id'];
            $Occupations_id = $_POST['Occupations_id'];

            $result = Requisites::saveRequisites($id, $Phone, $Fullname, $Email, $Fax, $Company_id, $Occupations_id);

            header("Location: /requisites/update/$id");

        }

        require_once (ROOT.'/views/panel/Reqisites/update.php');
        return true;
    }

    public function ActionDel($id)
    {
        User::checkAdmin();
        if(isset($_POST['submit'])){
            Requisites::DelReqisites($id);
            header("Location: /requisites");
        }
        require_once (ROOT.'/views/panel/Reqisites/Delete.php');
        return true;
    }
}