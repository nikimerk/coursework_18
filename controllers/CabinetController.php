<?php

class CabinetController
{

    public function actionIndex()
    {

        // Получаем идентификатор пользователя из сессии
        $userId = User::checkLogged();



        // Получаем информацию о пользователе из БД
        $user = User::getUserById($userId);


                
        require_once(ROOT . '/views/cabinet/index.php');

        return true;
    }  
    
    public function actionEdit()
    {
        // Получаем идентификатор пользователя из сессии
        $userId = User::checkLogged();
        
        // Получаем информацию о пользователе из БД
        $user = User::getUserById($userId);
        
        $name = $user['name'];
        $password = $user['password'];
                
        $result = false;     

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $password = $_POST['password'];
            
            $errors = false;
            
            if (!User::checkName($name)) {
                $errors[] = 'Имя не должно быть короче 2-х символов';
            }
            
            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 6-ти символов';
            }
            
            if ($errors == false) {
                $result = User::edit($userId, $name, $password);
            }

        }

        require_once(ROOT . '/views/cabinet/edit.php');

        return true;
    }

    public function ActionList(){
        User::checkLogged();
        $user =User::getUserById($_SESSION['user']);
        $ordersId = trim($user['order_number'], ",");


        $ordersList = Order::getOrderbyId($ordersId);

        require_once(ROOT . '/views/cabinet/ShopList.php');

        return true;
    }

    public function ActionView($id){
        User::checkLogged();
        $user =User::getUserById($_SESSION['user']);
        $ordersId = trim($user['order_number'], ",");
        $orders = explode(',', $ordersId);
        if(in_array($id, $orders)){


            $order = Order::getOrderbyId($id)[0];
            $productsQuantity  = json_decode($order['products'], true);
            $OrdersIds = array_keys($productsQuantity);
            $products = Product::getProdustsByIds($OrdersIds);

            require_once (ROOT.'/views/cabinet/view.php');
        }
        else{
        header("Location: /cabinet");
        die;}

        return true;
    }

    public function ActionDelete($id){
        User::checkLogged();
        $user =User::getUserById($_SESSION['user']);
        $ordersId = trim($user['order_number'], ",");
        $orders = explode(',', $ordersId);
        if(in_array($id, $orders)){
            if(isset($_POST['submit'])){
                Order::DelOrder($id);
                header("Location: /cabinet/shop_list");
            }
            require_once (ROOT.'/views/cabinet/delete.php');
        }
        else{
            header("Location: /cabinet");
            die;}

        return true;

    }

}