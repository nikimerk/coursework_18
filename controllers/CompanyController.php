<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 27.04.2018
 * Time: 7:47
 */

class CompanyController
{
    public function ActionIndex(){
        User::checkAdmin();
        $companyList = Company::getCompanyList();

        require_once (ROOT.'/views/panel/company/Index.php');
        return true;
    }

    public function ActionAdd()
    {
        User::checkAdmin();
        $Name = '';
        $Address= '';
        $result = false;

        if (isset($_POST['submit'])) {
            $Name = $_POST['Name'];
            $Address = $_POST['Address'];
            $errors = false;
            if (!User::checkName($Name)) {
                $errors[] = 'Имя не должно быть короче 2-х символов';
            }

            if ($errors == false) {
                $result = Company::Add($Name, $Address);
            }
        }
        require_once(ROOT . '/views/panel/Company/Add.php');
        return true;
    }

    public function ActionUpdate($id)
    {
        User::checkAdmin();

        $company = Doc::getCompanyById($id);
        $Name = $company['Name'];
        $Address = $company['Address'];


        if(isset($_POST['submit'])){

            $Name = $_POST['Name'];
            $Address = $_POST['Address'];


            $result = Company::saveCompany($id, $Name, $Address);

            header("Location: /company/update/$id");

        }

        require_once (ROOT.'/views/panel/company/update.php');
        return true;
    }

    public function ActionDel($id)
    {
        User::checkAdmin();
        if(isset($_POST['submit'])){
            Company::DelCompany($id);
            header("Location: /company");
        }
        require_once (ROOT.'/views/panel/company/delete.php');
        return true;
    }
}