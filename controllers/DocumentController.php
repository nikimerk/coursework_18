<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 24.04.2018
 * Time: 23:29
 */

class DocumentController
{
    public function actionIndex ()
    {
        $userId = User::checkLogged();
        $user = User::getUserById($userId);

        $documentsList =Doc::GetAllDocuments();
        require_once(ROOT . "/views/panel/Documents/ViewAllDoc.php");
        return true;
    }

    public function actionUpdate ($id)
    {

        $requisitesList = Requisites::getRequisitesList();
        $document = Doc::getDocumentById($id);
        $status2 = Doc::getStatusByIdALL($document['Status_id']);
        $StatusList = Doc::getStatusList();
//        print_r($StatusList);
        $id = $document['id'];
        $Name = $document['Name'];
        $Edit_date = $document['Edit_date'];
        $author2 = Requisites::getRequisiteById($document['Author_id']);
        $Status_id = $document['Status_id'];
        $Addressante2 = Requisites::getRequisiteById($document['Addressante_id']);
        $Docfield = Doc::getFieldById($document['Doc_fields_id']);


        $Doc_req_id = $document['Doc_req_id'];

        $DocTypeList = Doc::getTypeList();
        $doc_type2 = Doc::getTypeById($document['Doc_type_id']);

        $result = false;

        if (isset($_POST['submit'])) {


            $name = $_POST['Name'];
            $date = $_POST['Edit_date'];
            $Author_id = $_POST['Author_id'];
            $Status_id = $_POST['Status_id'];
            $Addressante_id = $_POST['Addressante_id'];
            $doc_type_id = $_POST['type'];
//            $Doc_req_id = $_POST['Doc_req_id'];
            $Field = $_POST['pole1'];
            $errors = false;

            if (!User::checkName($Name)) {
                $errors[] = 'Имя не должно быть короче 2-х символов';
            }


            if ($errors == false) {
                $fieldSave = Doc::saveFieldById($document['Doc_fields_id'], $Field);
                $result = Doc::updateDocument($id, $name, $date, $Author_id, $doc_type_id, $Addressante_id, $Status_id);
            }

        }


        require_once(ROOT . '/views/panel/Documents/Update.php');

        return true;
    }

    public function actionCreate()
    {
        $DocTypeList = Doc::getTypeList();
        $requisitesList = Requisites::getRequisitesList();
        if (isset($_POST['submit']))
        {
            $name = $_POST['Name'];
            $author = Requisites::getRequisiteById($_POST['Author_id']);
            $author_name = $author['Fullname'];
            $destination = Requisites::getRequisiteById($_POST['Destination_id']);
            $destination_name = $destination['Fullname'];
            $date = $_POST['Date'];
            $field = $_POST['pole1'];
            $occupations = Doc::getOccupationsById($destination['Occupations_id'])['Name'];
            $field_id = Doc::saveFields($field);
            $doc_type_id = $_POST['type'];
            $doc_type = Doc::getTypeById($doc_type_id)['Link'];




            $save = Doc::saveDocument($name, $date, $author['id'], $doc_type_id, $destination['id'], $field_id);

            if($save){
                header("Location: '/document");
            }
        }
        require_once(ROOT . '/views/panel/Documents/Create1.php');
        return true;
    }

    public function ActionDel($id)
    {
        if(isset($_POST['submit'])){
            Doc::DelDocument($id);
            Doc::delFieldById($id);
            header("Location: /document");
        }
        require_once (ROOT.'/views/panel/Documents/Delete.php');
        return true;
    }

    public function ActionDump()
    {
        include (ROOT.'/mysqldump-php-master/tests/test.php');


        $file = (ROOT."/dump_sql/db.sql");

        header ("Content-Type: application/octet-stream");
        header ("Accept-Ranges: bytes");
        header ("Content-Length: ".filesize($file));
        header ("Content-Disposition: attachment; filename=db.sql");
        readfile($file);

        header("Location: '/cabinet");
        return true;
    }

    public function ActionDownload($id)
    {
        $document = Doc::getDocumentById($id);
        $doc_type = Doc::getTypeById($document['Doc_type_id'])['Link'];
        $name = $document['Name'];
        $author = Requisites::getRequisiteById($document['Author_id']);
        $author_name = $author['Fullname'];
        $destination = Requisites::getRequisiteById($document['Addressante_id']);
        $destination_name = $destination['Fullname'];
        $date = $document['Edit_date'];

        $field = Doc::getFieldById($document['Doc_fields_id'])['Name'];
        $occupations = Doc::getOccupationsById($destination['Occupations_id'])['Name'];

//        print_r($destination_name);
        include_once (ROOT.'/php_stamp/index.php');
        $result = createDocument("C:/Xampp/htdocs/kurs/php_stamp/$doc_type", [
            'name' => "$name",
            'author_name' => "$author_name",
            'destination_name' => "$destination_name",
            'date' => "$date",
            'pole1' => "$field",
             'occupations' => "$occupations",
         ]);

            if (!$result) {
                echo 'Error';
            }


            return true;
    }
}