<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 24.04.2018
 * Time: 23:21
 */

class PanelController
{
    public function actionIndex ()
    {
        $userId = User::checkLogged();
        $user = User::getUserById($userId);

        require_once(ROOT . "/views/panel/Documents/ViewAllDoc.php");

        return true;

    }

}