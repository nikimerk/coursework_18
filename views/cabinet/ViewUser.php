<?php include ROOT . '/views/layouts/adminheader.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/cabinet">Кабинет пользователя</a></li>
                        <li class="active">Управление пользователями</li>
                    </ol>
                </div>

                <br/>


                <table class="table-bordered table-striped table">
                    <tr>
                        <th>ID пользователя</th>
                        <th>ФИО</th>
                        <th>Email</th>
                        <th>Роль</th>
                        <th>Назначить Администратором</th>
                        <th>Убрать права админа</th>
                        <th>Удалить пользователя</th>
                    </tr>
                    <?php foreach ($userList as $user): ?>
                        <tr>
                            <td><?php echo $user['id']; ?></td>
                            <td><?php echo $user['name']; ?></td>
                            <td><?php echo $user['Email']; ?></td>
                            <td><?php echo $user['role']; ?></td>
                            <td><a  href="/user/toadmin/<?php echo $user['id']; ?>" title="Редактировать"><i class="fa fa-pencil-square-o"></i></a></td>
                            <?php if ($user['role']=='admin'):?>
                           <td><a href="/user/deladmin/<?php echo $user['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
                                <td><a href="/user/deluser/<?php echo $user['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
    <?php endif;?>


                        </tr>
                    <?php endforeach; ?>
                </table>

            </div>
        </div>

    </section>

<?php include ROOT . '/views/layouts/adminfooter.php'; ?>