<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <h1>Кабинет пользователя</h1>
            
            <h3>Привет, <?php echo $user['name'];?>!</h3>
            <ul>
                <li><a href="/cabinet/edit">Редактировать данные</a></li>
                <li><a href="/document">Просмотр документов</a></li>
                <?php if ($user['role'] != 'admin'): ?>
                    <li><a href="/dump">Скачать дамп базы данных</a></li>
                <?php endif; ?>

                <?php if ($user['role'] == 'admin'): ?>
                    <hr>
                    Администрирование:
                    <li><a href="/requisites">Управление реквизитами</a></li>
                    <li><a href="/user/viewuser">Управление пользователями</a></li>
                    <li><a href="/company">Управление компаниями</a></li>
                <?php endif; ?>
            </ul>
            
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>