<?php include ROOT . '/views/layouts/adminheader.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/Cabinet">Кабинет пользователя</a></li>
                        <li><a href="/requisites/">Управление реквизитами</a></li>
                        <li class="active">Удалить реквизит</li>
                    </ol>
                </div>


                <h4>Удалить заказ #<?php echo $id; ?></h4>


                <p>Вы действительно хотите удалить этот реквизит?</p>

                <form method="post">
                    <input type="submit" name="submit" value="Удалить" />
                </form>
                <br>
                <br>
                <a href="/requisites/" class="btn btn-default back"><i class="fa fa-arrow-left"></i> Назад</a>
                <br/>
                <br/>
            </div>

        </div>

    </section>

<?php include ROOT . '/views/layouts/adminfooter.php'; ?>