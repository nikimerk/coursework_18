<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">


            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/cabinet">Кабинет пользователя</a></li>
                    <li><a href="/requisites">Управление реквизитами</a></li>
                    <li class="active">Редактирование реквизита</li>
                </ol>
            </div>


            <div class="col-sm-4 col-sm-offset-4 padding-right">
          

                <h4>Редактировать Реквизит #<?php echo $requisite['id'] ?></h4>
                <br>
                <div class="signup-form"><!--sign up form-->
                    <div class="login-form">
                        <form action="#" method="post">
                            <p>ФИО:</p>
                            <input type="text" name="Fullname" placeholder="ФИО:" value="<?php echo $requisite['Fullname'] ?>"/>

                            <p>Телефон:</p>
                            <input type="tel" name="Phone" placeholder="Телефон:" value="<?php echo $requisite['Phone']; ?>"/>

                            <p>Факс:</p>
                            <input type="text" name="Fax" placeholder="Факс:" value="<?php echo $requisite['Fax']; ?>"/>

                            <p>Email:</p>
                            <input type="email" name="Email" placeholder="Е-mail:" value="<?php echo $requisite['Email']; ?>"/>

                            <p>Выберете компанию:</p>
                            <select name="Company_id">
                                <?php foreach ($companyList as $company1): ?>
                                    <option value="<?php echo $company1['id'];?>" <?php if ($company2['id'] == $company1['id']) echo ' selected="selected"'; ?>> <?php echo $company1['Name']; ?> </option>
                                <?php endforeach; ?>
                            </select>
                            <br>
                            <br>
                            <p>Выберете должность:</p>
                            <select name="Occupations_id">
                                <?php foreach ($occupationsList as $occupations1): ?>
                                    <option value="<?php echo $occupations1['id'];?>" <?php if ($occupations2['id'] == $occupations1['id']) echo ' selected="selected"'; ?>> <?php echo $occupations1['Name']; ?> </option>
                                <?php endforeach; ?>
                            </select>

                            <br>
                            <br>
                            <input type="submit" name="submit" class="btn btn-default" value="Сохранить">
                        </form>
                    </div>

                </div><!--/sign up form-->


                <br/>
                <br/>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>





