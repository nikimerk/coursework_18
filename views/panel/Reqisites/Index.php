<?php include ROOT . '/views/layouts/adminheader.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/cabinet">Кабинет пользователя</a></li>
                        <li class="active">Управление реквизитами</li>
                    </ol>
                </div>



                <br/>


                <table class="table-bordered table-striped table">
                    <tr>
                        <th>ID Реквезита</th>
                        <th>ФИО</th>
                        <th>Телефон</th>

                        <th>Email</th>
                        <th>Факс</th>
                        <th>Компания</th>
                        <th>Должность</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <?php foreach ($requisitesList as $requisites): ?>
                        <tr>
                            <td><?php echo $requisites['id']; ?></td>
                            <td><?php echo $requisites['Fullname']; ?></td>
                            <td><?php echo $requisites['Phone']; ?></td>
                            <td><?php echo $requisites['Email']; ?></td>
                            <td><?php echo $requisites['Fax']; ?></td>
                            <td><?php echo ( Doc::getCompanyById($requisites['Company_id'])['Name']); ?></td>
                            <td><?php echo ( Doc::getOccupationsById($requisites['Occupations_id'])['Name']); ?></td>
                            <td><a href="/requisites/update/<?php echo $requisites['id']; ?>" title="Редактировать"><i class="fa fa-pencil-square-o"></i></a></td>
                            <td><a href="/requisites/del/<?php echo $requisites['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
                        </tr>
                    <?php endforeach; ?>
                    <td><a href="/requisites/add">Добавить реквизит</a></td>
                </table>

            </div>
        </div>

    </section>

<?php include ROOT . '/views/layouts/adminfooter.php'; ?>