<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <div class="row">


                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/cabinet">Кабинет пользователя</a></li>
                        <li><a href="/requisites">Управление реквизитами</a></li>
                        <li class="active">Добавление реквизита</li>
                    </ol>
                </div>


                <div class="col-sm-4 col-sm-offset-4 padding-right">

                    <?php if ($result): ?>
                        <p>Реквезиты успешно добавлены!</p>
                    <?php else: ?>
                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li> - <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>




                        <div class="signup-form"><!--sign up form-->

                            <div class="login-form">
                                <form action="#" method="post">

                                    <p>ФИО:</p>
                                    <input type="text" name="Fullname" placeholder="ФИО:" value="<?php echo $Fullname ?>"/>

                                    <p>Телефон:</p>
                                    <input type="tel" name="Phone" placeholder="Телефон:" value="<?php echo $Phone; ?>"/>

                                    <p>Факс:</p>
                                    <input type="text" name="Fax" placeholder="Факс:" value="<?php echo $Fax; ?>"/>

                                    <p>Email:</p>
                                    <input type="email" name="Email" placeholder="Е-mail:" value="<?php echo $Email; ?>"/>

                                    <p>Выберете компанию:</p>
                                    <select name="Company_id">
                                        <?php foreach ($companyList as $company): ?>
                                            <option value="<?php echo $company['id'];?>"> <?php echo $company['Name']; ?> </option>
                                        <?php endforeach; ?>
                                    </select>
                                    <br>
                                    <br>
                                    <p>Выберете должность:</p>
                                    <select name="Occupations_id">
                                        <?php foreach ($occupationsList as $occupations): ?>
                                            <option value="<?php echo $occupations['id'];?>"> <?php echo $occupations['Name']; ?> </option>
                                        <?php endforeach; ?>
                                    </select>
                                    <br>

                                    <br>
                                    <input type="submit" name="submit" class="btn btn-default" value="Добавить">
                                </form>
                            </div>

                            <?php endif; ?>
                        </div><!--/sign up form-->


                    <br/>
                    <br/>
                </div>
            </div>
        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>