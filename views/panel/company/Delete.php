<?php include ROOT . '/views/layouts/adminheader.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/Cabinet">Кабинет пользователя</a></li>
                        <li><a href="/company">Управление компаниями</a></li>
                        <li class="active">Удалить компанию</li>
                    </ol>
                </div>


                <h4>Удалить компанию #<?php echo $id; ?></h4>


                <p>Вы действительно хотите удалить эту компанию?</p>

                <form method="post">
                    <input type="submit" name="submit" value="Удалить" />
                </form>
                <br>
                <br>
                <a href="/company" class="btn btn-default back"><i class="fa fa-arrow-left"></i> Назад</a>
                <br/>
                <br/>
            </div>

        </div>

    </section>

<?php include ROOT . '/views/layouts/adminfooter.php'; ?>