<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">


            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/cabinet">Кабинет пользователя</a></li>
                    <li><a href="/company">Управление компаниями</a></li>
                    <li class="active">Редактирование компании</li>
                </ol>
            </div>


            <div class="col-sm-4 col-sm-offset-4 padding-right">
          

                <h4>Редактировать Компанию #<?php echo $id ?></h4>
                <br>
                <div class="signup-form"><!--sign up form-->
                    <div class="login-form">
                        <form action="#" method="post">
                            <p>Название:</p>
                            <input type="text" name="Name" placeholder="Название:" value="<?php echo $Name; ?>"/>

                            <p>Адрес:</p>
                            <input type="text" name="Address" placeholder="Адрес:" value="<?php echo $Address; ?>"/>

                            <br>
                            <br>
                            <input type="submit" name="submit" class="btn btn-default" value="Сохранить">
                        </form>
                    </div>

                </div><!--/sign up form-->


                <br/>
                <br/>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>





