<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <div class="row">


                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/cabinet">Кабинет пользователя</a></li>
                        <li><a href="/company">Управление компаниями</a></li>
                        <li class="active">Добавление компании</li>
                    </ol>
                </div>


                <div class="col-sm-4 col-sm-offset-4 padding-right">

                    <?php if ($result): ?>
                        <p>Компания успешно добавлена!</p>
                    <?php else: ?>
                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li> - <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>




                        <div class="signup-form"><!--sign up form-->

                            <div class="login-form">
                                <form action="#" method="post">

                                    <p>Название:</p>
                                    <input type="text" name="Name" placeholder="Название:" value="<?php echo $Name ?>"/>

                                    <p>Адрес:</p>
                                    <input type="text" name="Address" placeholder="Адрес:" value="<?php echo $Address; ?>"/>

                                    <br>
                                    <input type="submit" name="submit" class="btn btn-default" value="Добавить">
                                </form>
                            </div>

                            <?php endif; ?>
                        </div><!--/sign up form-->


                    <br/>
                    <br/>
                </div>
            </div>
        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>