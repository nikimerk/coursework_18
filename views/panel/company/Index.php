<?php include ROOT . '/views/layouts/adminheader.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/cabinet">Кабинет пользователя</a></li>
                        <li class="active">Управление Компаниями</li>
                    </ol>
                </div>



                <br/>


                <table class="table-bordered table-striped table">
                    <tr>
                        <th>ID Компании</th>
                        <th>Название</th>
                        <th>Адрес</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <?php foreach ($companyList as $company): ?>
                        <tr>
                            <td><?php echo $company['id']; ?></td>
                            <td><?php echo $company['Name']; ?></td>
                            <td><?php echo $company['Address']; ?></td>
                            <td><a href="/company/update/<?php echo $company['id']; ?>" title="Редактировать"><i class="fa fa-pencil-square-o"></i></a></td>
                            <td><a href="/company/del/<?php echo $company['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
                        </tr>
                    <?php endforeach; ?>
                    <td><a href="/company/add">Добавить компанию</a></td>
                </table>

            </div>
        </div>

    </section>

<?php include ROOT . '/views/layouts/adminfooter.php'; ?>