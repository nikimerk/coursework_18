<?php include ROOT . '/views/layouts/header.php'; ?>
<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/cabinet">Кабинет пользователя</a></li>
                    <li class="active">Просмотр документов</li>
                </ol>
            </div>



            <br/>



            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID</th>
                    <th>Название </th>
                    <th>Формат </th>
                    <th>Автор</th>
                    <th>Дата</th>
                    <th>Статус</th>
                    <th>Скачать</th>
                    <th>Редактировать</th>
                    <th>Удалить</th>
                </tr>
                <?php foreach ($documentsList as $document): ?>
                    <tr>
                        <td><?php echo $document['id']; ?></td>
                        <td><?php echo $document['Name']; ?></td>
                        <td><?php echo Doc::getTypeById($document['Doc_type_id'])['Name'];; ?></td>
                        <td><?php echo Requisites::getRequisiteById($document['Author_id'])['Fullname']; ?></td>
                        <td><?php echo $document['Edit_date']; ?></td>
                        <td><?php echo Doc::getStatusById($document['Status_id']); ?></td>
                        <td><a href="/document/download/<?php echo $document['id']; ?>" title="Скачать"><i class="fa fa-eye"></i></a></td>
                        <td><a href="/document/update/<?php echo $document['id']; ?>" title="Редактировать"><i class="fa fa-pencil-square-o"></i></a></td>
                        <td><a href="/document/del/<?php echo $document['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
                    </tr>
                <?php endforeach; ?>
                <td><a href="/document/create">Создать документ</a></td>
            </table>

        </div>
    </div>

</section>
<?php include ROOT . '/views/layouts/footer.php'; ?>