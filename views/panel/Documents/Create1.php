<?php
//echo "<pre>";
//print_r($requisitesList);
include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <div class="row">


                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/cabinet">Кабинет пользователя</a></li>
                        <li><a href="/document">Управление документами</a></li>
                        <li class="active">Добавление документы</li>
                    </ol>
                </div>



                <div class="col-sm-4 col-sm-offset-4 padding-right">

<!--                    --><?php //if ($result): ?>
<!--                        <p>Реквезиты успешно добавлены!</p>-->
<!--                    --><?php //else: ?>
<!--                    --><?php //if (isset($errors) && is_array($errors)): ?>
<!--                        <ul>-->
<!--                            --><?php //foreach ($errors as $error): ?>
<!--                                <li> - --><?php //echo $error; ?><!--</li>-->
<!--                            --><?php //endforeach; ?>
<!--                        </ul>-->
<!--                    --><?php //endif; ?>




                    <div class="signup-form"><!--sign up form-->

                        <div class="login-form">
                            <form action="#" method="post">

                                <p>Формат:</p>
                                <select name="type">
                                    <?php foreach ($DocTypeList as $DocType): ?>
                                        <option value="<?php echo $DocType['id'];?>"> <?php echo $DocType['Name']; ?> </option>
                                    <?php endforeach; ?>
                                </select>
                                <br>
                                <br>
                                <p>Название документа:</p>
                                <input type="text" name="Name" placeholder="Название документа:" value=""/>

                                <p>Автор:</p>
                                <select name="Author_id">
                                    <?php foreach ($requisitesList as $requisites): ?>
                                        <option value="<?php echo $requisites['id'];?>"> <?php echo $requisites['Fullname']; ?> </option>
                                    <?php endforeach; ?>
                                </select>

                                <p>Адресат:</p>
                                <select name="Destination_id">
                                    <?php foreach ($requisitesList as $requisites): ?>
                                        <option value="<?php echo $requisites['id'];?>"> <?php echo $requisites['Fullname']; ?> </option>
                                    <?php endforeach; ?>
                                </select>

                                <p>Дата:</p>
                                <input type="date" name="Date" value=""/>

                                <p>Поле1:</p>
                                <textarea name="pole1" cols="40" rows="5"></textarea>




                                <br>
                                <br>
                                <input type="submit" name="submit" class="btn btn-default" value="Добавить">
                            </form>
                        </div>

<!--                        --><?php //endif; ?>
                    </div><!--/sign up form-->


                    <br/>
                    <br/>
                </div>
            </div>
        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>