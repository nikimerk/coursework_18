<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">


            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/cabinet">Кабинет пользователя</a></li>
                    <li><a href="/document">Управление Документами</a></li>
                    <li class="active">Редактирование документ</li>
                </ol>
            </div>


            <div class="col-sm-4 col-sm-offset-4 padding-right">
          

                <h4>Редактировать Реквизит #<?php echo $document['id'] ?></h4>
                <br>
                <div class="signup-form"><!--sign up form-->
                    <div class="login-form">
                        <form action="#" method="post">
                            <p>Формат:</p>
                            <select name="type">
                                <?php foreach ($DocTypeList as $DocType1): ?>
                                    <option value="<?php echo $DocType1['id'];?>"<?php if ($doc_type2['id'] == $DocType1['id']) echo ' selected="selected"'; ?>> <?php echo $DocType1['Name']; ?> </option>
                                <?php endforeach; ?>
                            </select>
                            <br>
                            <br>
                            <p>Название документа:</p>
                            <input type="text" name="Name" placeholder="Название документа:" value="<?=$Name?>"/>

                            <p>Автор:</p>
                            <select name="Author_id">
                                <?php foreach ($requisitesList as $requisites1): ?>
                                    <option value="<?php echo $requisites1['id'];?>" <?php if ($author2['id'] == $requisites1['id']) echo ' selected="selected"'; ?>> <?php echo $requisites1['Fullname']; ?> </option>
                                <?php endforeach; ?>
                            </select>

                            <p>Адресат:</p>
                            <select name="Addressante_id">

                                <?php foreach ($requisitesList as $requisites1): ?>
                                    <option value="<?php echo $requisites1['id'];?>"<?php if ($Addressante2['id'] == $requisites1['id']) echo ' selected="selected"'; ?>> <?php echo $requisites1['Fullname']; ?> </option>
                                <?php endforeach; ?>
                            </select>

                            <p>Статус:</p>
                            <select name="Status_id">
                                <?php foreach ($StatusList as $status1): ?>
                                    <option value="<?php echo $status1['id'];?>"<?php if ($status2['id'] ==$status1['id'])echo ' selected="selected"'; ?>> <?php echo $status1['Status']; ?> </option>
                                <?php endforeach; ?>
                            </select>

                            <p>Дата:</p>
                            <input type="date" name="Edit_date" value="<?=$Edit_date?>"/>

                            <p>Поле1:</p>
                            <textarea name="pole1" cols="40" rows="5" > <?=$Docfield['Name']?> </textarea>



                            <br>
                            <br>
                            <input type="submit" name="submit" class="btn btn-default" value="Сохранить">
                        </form>
                    </div>

                </div><!--/sign up form-->


                <br/>
                <br/>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>





