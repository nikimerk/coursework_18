<?php include ROOT . '/views/layouts/header.php'; ?>
    <section>
        <div class="container">
            <div class="row">

                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/admin">Кабинет пользователя</a></li>
                        <li class="active">Просмотр документов</li>
                    </ol>
                </div>

                <h4>Список заказов</h4>

                <br/>



                <table class="table-bordered table-striped table">
                    <tr>
                        <th>ID</th>
                        <th>Название </th>
                        <th>Автор</th>
                        <th>Дата</th>
                        <th>Статус<                                                             /th>
                        <th>Скачать</th>
                        <th>Редактировать</th>
                        <th>Удалить</th>
                    </tr>
                    <?php foreach ($documentsList as $document): ?>
                        <tr>
                            <td><?php echo $document['id']; ?></td>
                            <td><?php echo $document['Name']; ?></td>
                            <td><?php echo $document['Author_id']; ?></td>
                            <td><?php echo $document['Edit_date']; ?></td>
                            <td><?php echo Doc::getStatus($document['Status_id']); ?></td>
                            <td><a href="/admin/order/view/<?php echo $document['id']; ?>" title="Смотреть"><i class="fa fa-eye"></i></a></td>
                            <td><a href="/admin/order/update/<?php echo $document['id']; ?>" title="Редактировать"><i class="fa fa-pencil-square-o"></i></a></td>
                            <td><a href="/admin/order/delete/<?php echo $document['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
                        </tr>
                    <?php endforeach; ?>
                </table>

            </div>
        </div>

    </section>
<?php include ROOT . '/views/layouts/footer.php'; ?>